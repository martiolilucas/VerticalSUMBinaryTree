﻿using BinaryTree;
using System.Collections.Generic;

namespace VerticalSUMBinaryTree
{
    class VerticalSUMBinaryTree
    {
        public Dictionary<int, int> HorizontalDisctances { get; }

        public VerticalSUMBinaryTree(BinaryTree<int> tree)
        {
            HorizontalDisctances = new Dictionary<int, int>();
            SumVerticalDistance(tree.Root, 0);
        }

        private void SumVerticalDistance(BinaryTreeNode<int> node, int counter)
        {

            var value = 0;
            if (node != null)
            {
                value = node.Value;
                if (node.Left != null)
                    SumVerticalDistance(node.Left, counter - 1);

                if (node.Right != null)
                    SumVerticalDistance(node.Right, counter + 1);
            }


            if (HorizontalDisctances.ContainsKey(counter))
            {
                HorizontalDisctances[counter] += value;
            }
            else
            {
                HorizontalDisctances.Add(counter, value);
            }
        }
    }
}