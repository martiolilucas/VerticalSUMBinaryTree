﻿using BinaryTree;
using System;

namespace VerticalSUMBinaryTree
{
    class Program
    {
        static void Main(string[] args)
        {

            var bt = new BinaryTree<int>();
            bt.Root = new BinaryTreeNode<int>(1);
            bt.Root.Left = new BinaryTreeNode<int>(2);
            bt.Root.Right = new BinaryTreeNode<int>(3);

            var vsbt = new VerticalSUMBinaryTree(bt);

            Console.WriteLine("Result:");

            foreach (var horizontalDisctance in vsbt.HorizontalDisctances)
            {
                Console.WriteLine(horizontalDisctance.Key + " => " + horizontalDisctance.Value);
            }

        }

    }

}

